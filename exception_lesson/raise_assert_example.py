# raise --> Exception

# x = 10
#
# if x > 5:
#     raise Exception("x should not exceed 5")

# assert (False) --> AssertionError

import sys

assert ('win' in sys.platform), "This code runs no Windows only"
assert ('linux' in sys.platform), "This code runs no Linux only"