import hr
import employees
import prouctivity

manager = employees.Manager(1, 'Semen Tomenko', 1500)
secretary = employees.Secretary(4, "Ivan Donko", 1000)
factory_worker = employees.FactoryWorker(2, 'Katerina Semenova', 40, 15)
sales = employees.SalesPerson(3, 'Olga Evko', 1000, 250)
temporary_secretary = employees.TemporarySecretary(5, "Fedor Lutsenko", 40, 9)

employees_list = [
    manager,
    secretary,
    sales,
    factory_worker,
    temporary_secretary
]

productivity_system = prouctivity.ProductivitySystem()
productivity_system.track(employees_list, 40)
payroll_system = hr.PayrollSystem()
payroll_system.calculate_payroll(employees_list)